# File icons for the Ranger file manager in kitty

This plugin introduces a new linemode that prefixes file names with a file icon.  
This fork of the [original repo](https://github.com/alexanderjeurissen/ranger_devicons) accounts
for kitty-specific cutting out / sizing issues with glyphs wider than one cell.

![image](screenshot.png)

## Prerequisites
This plugin uses glyphs from a patched NERDfont. So in order for this plugin to work you need to
install a NERDfont and set it as the default font for your terminal.

In kitty, the process of installing is a bit more involved than just following instructions in the
[NERDfont repo](https://github.com/ryanoasis/nerd-fonts), at least at the time of writing. 
([related issue #1](https://github.com/ryanoasis/nerd-fonts/issues/268), 
[related issue #2](https://github.com/kovidgoyal/kitty/issues/527)).

There are multiple ways to get around this, in the screenshot I use an unpatched font and the 
[ttf-nerd-fonts-symbols aur package](https://aur.archlinux.org/packages/ttf-nerd-fonts-symbols/).

## Install instructions
### Stable method:
A makefile is included to install and uninstall this plugin. to install simply run:
`make install` and to uninstall the plugin run `make uninstall`.

### Experimental method:
Ranger has added support for loading directories in the plugins folder to `master` which makes it
easier to install and keep plugins updated.  
To install, just clone the repo into the plugins folder:
```bash
git clone https://gitlab.com/ww7w/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
```
Then add `default_linemode devicons` to your `rc.conf`.
